#!/bin/sh

capture_time=600
iterations=$(expr $capture_time / 20)

IP=$(python get_my_ip.py)
echo local_ip = $IP

# cur_time=$(echo| eval date +%Y%m%d%H%M)
# dir='../data/logs/emulator'
# mkdir $dir/$cur_time
# dir=$dir/$cur_time

BW="1500Kbit/s"

# Set bandwidth pipe with dummynet
sudo ipfw -q flush
sudo ipfw add pipe 1 in proto tcp
sudo ipfw pipe 1 config bw $BW delay 5ms
sudo ipfw pipe list

dir=201303121417/1500Kbit_s/fixed/

bitrate=1750
mkdir $dir/$bitrate
tcpdump -n -tt -A -i en1 src $IP and port 80 and greater 100 > $dir/$bitrate/outgoingLog.txt &
tcpdump -n -tt -e -i en1 dst $IP and port 80 > $dir/$bitrate/incomingLog.txt &
sleep 5

# Run Netflix Emulator - fixed rate
python netflix_emulator.py -d -m constant-bitrate --buffersize 100 --bitrate=$bitrate \
    --write-buffer-usage --buffer-usage-output-file $dir/$bitrate/buffer.log > $dir/$bitrate/output.txt &

# Check programs using network
for ((i=0; i <= $iterations; i++))
do
    lsof -n -i | grep ESTABLISHED >> $dir/$bitrate/lsofLog_tcp.txt &
    sleep 10
done

# Start wget download
python wget_wrapper.py &
sleep 10

for ((i=0; i <= $iterations; i++))
do
    lsof -n -i | grep ESTABLISHED >> $dir/$bitrate/lsofLog_tcp.txt &
    sleep 10
done
killall Python
killall tcpdump
killall wget
rm 0*

python plot_results.py $dir/$bitrate &

echo -en "\007"
sudo ipfw -f flush
sudo ipfw -f pipe flush




