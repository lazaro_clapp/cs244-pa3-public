#!/usr/bin/python

"CS244 Spring 2013 Assignment 3: Netflix"

import math
import os
import re
import sys

## Regular expressions for log parsing
outgoing_entry_header_re = re.compile(r'^(?P<time_str>\d+.\d{6}) IP (?P<local_ip>(\d{1,3}\.){3}\d{1,3})\.\d+ > (?P<remote_ip>(\d{1,3}\.){3}\d{1,3})\.\d+:.*$')
outgoing_entry_get_re = re.compile(r'^.*GET /(?P<fname>\d+.(ismv|isma))/range/(?P<range_start>\d+)-(?P<range_end>\d+)\?(?P<token>.*) HTTP.*$')

def parse_tokens(outgoingFile):
    tokens = {}
    with open(outgoingFile) as f:
        line = "___"
        while line != "":
            line = f.readline()
            header_match = outgoing_entry_header_re.match(line)
            if header_match == None: continue
            http_get_match = None
            while (http_get_match == None and line != ""):
                line = f.readline()
                if line == "\n" or line == "\r\n":
                     line = f.readline()
                     if line == "\n" or line == "\r\n": break
                http_get_match = outgoing_entry_get_re.match(line)
            
            if http_get_match == None: continue
            fname = http_get_match.group("fname")
            if fname not in tokens:
                l = [http_get_match.group("token"), 
                     header_match.group("local_ip"),
                     header_match.group("remote_ip")]
                tokens[fname] = l
    return tokens

def main(outgoingFile):
    for f, l in parse_tokens(outgoingFile).items():
        print "|".join([f] + l)
    
if __name__ == "__main__":
    main(sys.argv[1])
