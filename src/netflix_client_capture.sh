#!/bin/sh

capture_time=500
iterations=$(expr $capture_time / 20)

IP=$(python get_my_ip.py)
echo local_ip = $IP

cur_time=$(echo| eval date +%Y%m%d%H%M)
mkdir real_netflix
dir='real_netflix'
mkdir $dir/$cur_time
dir=$dir/$cur_time

# Convert passed argument to bandwidth string
str="Kbit/s"
if (( $# )) || (( ($1 != 0) ))
then
    BW=$1$str
else
    BW="1Mbit/s"
fi
echo Bandwidth = $BW

# Set bandwidth pipe with dummynet
sudo ipfw -q flush
sudo ipfw add pipe 1 in proto tcp
sudo ipfw pipe 1 config bw $BW delay 5ms
sudo ipfw pipe list

# Start TCP Dump
tcpdump -n -tt -A -i en1 src $IP and port 80 and greater 100 > $dir/outgoingLog.txt &
tcpdump -n -tt -e -i en1 dst $IP and port 80 > $dir/incomingLog.txt &
sleep 5

# User starts Netflix client
read -p "Press Enter after starting Netflix Client"
sleep 10

# Check programs using network
for ((i=0; i <= $iterations; i++))
do
    lsof -n -i | grep ESTABLISHED >> $dir/$bitrate/lsofLog_tcp.txt &
    sleep 10
done

# Start wget download
python wget_wrapper.py &
sleep 10
for ((i=0; i <= $iterations; i++))
do
    lsof -n -i | grep ESTABLISHED >> $dir/$bitrate/lsofLog_tcp.txt &
    sleep 10
done
killall Python
killall tcpdump
killall wget
rm 0*

# Plot results
python plot_results.py $dir &

echo -en "\007"
sudo ipfw -f flush
sudo ipfw -f pipe flush