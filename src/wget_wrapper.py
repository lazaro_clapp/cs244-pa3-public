import subprocess
import os
from optparse import OptionParser

BITRATES_MAP_FILE = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir, "data", "bitrates.map"))
TOKENS_MAP_FILE = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir, "data", "tokens.map"))

WGET_COMMAND = "wget"
URL_TEMPLATE = "http://%s/%s/range/0-?%s"

def parseCmdLine():
    usage = "usage: %prog [options]"
    parser = OptionParser(usage)
    parser.add_option("--bitrate", dest="bitRate", default="3000", type="string", 
                      help="For emulators using a fixed bitrate.", metavar="[bitrate]")
    return parser.parse_args()
    
class WgetWrapper(object):

    def __init__(self, options):
        self.bitrate = options.bitRate
        self._remoteip = None
        self._bitratesmap = None
        self._tokenmap = None

    def get_bitratesmap(self):
        if self._bitratesmap != None: return self._bitratesmap
        d = {}
        with open(BITRATES_MAP_FILE) as f:
            for line in f:
                line = line.strip()
                if line == "": break
                fname, kbps = line.split('|')
                d[kbps] = fname
        self._bitratesmap = d
        return self._bitratesmap
        
    def get_token_map(self):
        if self._tokenmap != None: return self._tokenmap
        d = {}
        with open(TOKENS_MAP_FILE) as f:
            for line in f:
                line = line.strip()
                if line == "": break
                fname, token, local_ip, remote_ip = line.split('|')
                self._remoteip = remote_ip
                d[fname] = token
        self._tokenmap = d
        return self._tokenmap

    def get_service_ip(self):
        if self._remoteip != None: return self._remoteip
        self.get_token_map() #get_token_map sets the remote IP as a side effect
        return self._remoteip
    
    def get_url(self):
        service_ip = self.get_service_ip()
        fname = self.get_bitratesmap()[self.bitrate]
        token = self.get_token_map()[fname]
        return (URL_TEMPLATE % (service_ip, fname, token))
    
    def run(self):
        subprocess.call([WGET_COMMAND, self.get_url()])
      
def run_wget_wrapper(options):
    wget = WgetWrapper(options)
    wget.run()
        
if __name__ == "__main__":
    (options, args) = parseCmdLine()
    run_wget_wrapper(options)
