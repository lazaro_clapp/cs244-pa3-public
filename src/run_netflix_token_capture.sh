#!/bin/sh

IP=$(python get_my_ip.py)
echo local_ip = $IP

tcpdump -n -tt -A -i en1 src $IP and port 80 and greater 100 > ../data/tokens/tokens.txt &
sleep 2
echo "Start Netflix Client and play first 10 seconds for each bitrate"
read -p "Press Enter to stop capture..."
killall tcpdump

echo "Creating token map"
python extract_tokens.py ../data/tokens/tokens.txt > ../data/tokens.map