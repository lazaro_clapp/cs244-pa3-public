#!/bin/sh

BW=$1
BW.="Mbit/s"

sudo ipfw -q flush
sudo ipfw add pipe 1 in proto tcp
sudo ipfw pipe 1 config bw $BW delay 5ms
sudo ipfw pipe list
read -p "Enter to clear the bandwidth setting..."
sudo ipfw -f flush
sudo ipfw pipe flush