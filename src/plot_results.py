#!/usr/bin/python

"CS244 Spring 2013 Assignment 3: Netflix"

import math
import os
import re
import sys

import matplotlib.pyplot as plt

## Constants:
SECS_TO_AGGREGATE = 3
LSOF_LOG_FILENAME = "lsofLog_tcp.txt"
INCOMING_LOG_FILENAME = "incomingLog.txt"
OUTGOING_LOG_FILENAME = "outgoingLog.txt"
BUFFER_LOG_FILENAME = "buffer.log"
PLOT_FILENAME = "plot.png"
BUFFER_PLOT_FILENAME = "buffer_plot.png"
NETFLIX_CLIENT_APPNAME = "Google"
NETFILX_CLIENT_EMULATOR_APPNAME = "Python"
COMPETING_APPNAME = "wget"
AUDIO_BUFFER_TAG = "audioBuffer"
VIDEO_BUFFER_TAG = "videoBuffer"

## Get bitrates.map file, location relative to this file.
BITRATES_MAP_FILE = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir, "data", "bitrates.map"))
TOKENS_MAP_FILE = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir, "data", "tokens.map"))

## Regular expressions for log parsing
lsof_re = re.compile(r'^(?P<app_name>\w+) .* TCP (?P<local_ip>(\d{1,3}\.){3}\d{1,3}):(?P<local_port>\d+)->(?P<remote_ip>(\d{1,3}\.){3}\d{1,3}):.*$')
incoming_entry_re = re.compile(r'^(?P<time_str>\d+.\d{6}).*length (?P<length>\d+): (?P<remote_ip>(\d{1,3}\.){4}\d+) > (?P<local_ip>(\d{1,3}\.){4}\d+):.*$')
outgoing_entry_header_re = re.compile(r'^(?P<time_str>\d+.\d{6}) IP (?P<remote_ip>(\d{1,3}\.){4}\d+) > (?P<local_ip>(\d{1,3}\.){4}\d+):.*$')
outgoing_entry_get_re = re.compile(r'^.*GET /(?P<fname>\d+.ismv)/range/(?P<range_start>\d+)-(?P<range_end>\d+)\?.*$')

## Global variables representing the minimum and maximum values of the x-axis (time in seconds) of the plot
## Intialized to empty range, updates as we read in time values.
min_sec = 1000000000000
max_sec = 0

## Classes and data structures
class IncomingTcpDumEntry:

    def __init__(self, time, remote, local, length):
        self.time = time
        self.remote = remote
        self.local = local
        self.length = length
        
    def __str__(self):
        return "{ " + " | ".join([str(self.time), str(self.remote), str(self.local), str(self.length)]) + " }"

class OutgoingTcpDumEntry:

    def __init__(self, time, remote, local, filename):
        self.time = time
        self.remote = remote
        self.local = local
        self.filename = filename
        
    def __str__(self):
        return "{ " + " | ".join([str(self.time), str(self.remote), str(self.local), str(self.filename)]) + " }"

class Time:
   
    def __init__(self, s, ns):
        self.s = s
        self.ns = ns
    
    def __str__(self):
        return ".".join([self.s, self.ns])
        
def timestr_to_ns(timestr):
    s, ns = timestr.split('.')
    return Time(int(s), int(ns))
        
class Address:

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        
    def __str__(self):
        return self.ip + ":" + self.port
        
def str_to_address(ipstr):
    ip1, ip2, ip3, ip4, port = ipstr.split(".")
    ip = ".".join([ip1, ip2, ip3, ip4])
    return Address(ip, port)

## Log files' parsing functions:

def get_service_ip():
    with open(TOKENS_MAP_FILE) as f:
        line = f.readline().strip()
        fname, token, local_ip, remote_ip = line.split('|')
        return remote_ip

def grab_ports(lsofFile, remote_ip):
    '''Read the lsof log file and get the port numbers for both the app flow 
    and the competing flow.'''
    netflix_ports = []
    wget_ports = []
    with open(lsofFile) as f:
        for line in f:
            lsof_match = lsof_re.match(line)
            assert lsof_match != None
            if lsof_match.group("remote_ip") != remote_ip: continue
            
            app_name = lsof_match.group("app_name")
            local_port = lsof_match.group("local_port")
            
            if app_name == NETFLIX_CLIENT_APPNAME:
                netflix_ports.append(local_port)
            if app_name == NETFILX_CLIENT_EMULATOR_APPNAME:
                netflix_ports.append(local_port)
            elif app_name == COMPETING_APPNAME:
                wget_ports.append(local_port)
                
    return (netflix_ports, wget_ports) 

def read_incoming_log(incomingFile):
    global min_sec; global max_sec
    
    log = []
    with open(incomingFile) as f:
        for line in f:
            if line == "\n": break
            entry_match = incoming_entry_re.match(line)
            assert entry_match != None
            entry = IncomingTcpDumEntry(timestr_to_ns(entry_match.group("time_str")), 
                              str_to_address(entry_match.group("remote_ip")), 
                              str_to_address(entry_match.group("local_ip")), 
                              int(entry_match.group("length")))
            log.append(entry)
            min_sec = min(min_sec, entry.time.s)
            max_sec = max(max_sec, entry.time.s)
    return log

def read_outgoing_log(outgoingFile):
    global min_sec; global max_sec
    log = []
    with open(outgoingFile) as f:
        line = "___"
        while line != "":
            line = f.readline()
            header_match = outgoing_entry_header_re.match(line)
            if header_match == None: continue
            http_get_match = None
            while (http_get_match == None and line != ""):
                line = f.readline()
                if line == "\n" or line == "\r\n":
                     line = f.readline()
                     if line == "\n" or line == "\r\n": break
                http_get_match = outgoing_entry_get_re.match(line)
            
            if http_get_match == None: continue
            entry = OutgoingTcpDumEntry(timestr_to_ns(header_match.group("time_str")), 
                              str_to_address(header_match.group("remote_ip")), 
                              str_to_address(header_match.group("local_ip")), 
                              http_get_match.group("fname"))
            log.append(entry)
            min_sec = min(min_sec, entry.time.s)
            max_sec = max(max_sec, entry.time.s)
    return log
                          
def filter_by_remote_ip(entries, remote_ip):
    for e in entries:
        if e.remote.ip == remote_ip:
            yield e

def aggregate_by_ports(entries, remote_ip, port_list):
    # returns {local_port => [entry]}
    d = {}
    for e in filter_by_remote_ip(entries, remote_ip):
        port = e.local.port
        if port not in port_list: continue
        if port not in d:
            d[port] = []
        d[port].append(e)
    return d

def aggregate_by_second(entries):
    # returns [sec => SUM(length)]
    d = {}
    for e in entries:
        t = e.time
        sec = t.s
        if sec not in d:
            d[sec] = 0
        d[sec] += e.length
    return d    

def get_bitratesmap():
    d = {}
    with open(BITRATES_MAP_FILE) as f:
        for line in f:
            if line == "": break
            fname, kbps = line.split('|')
            d[fname] = int(kbps)
    return d    

def get_emulator_type(logsDir):
    bufferlog = os.path.join(logsDir, BUFFER_LOG_FILENAME)
    if not os.path.exists(bufferlog):
        return "Real \"Service A\" Client (no emulator tag)"
    with open(bufferlog) as f:
        line = f.readline()
        assert line.startswith("#")
        return line[1:]
        

def get_buffer_info(bufferlog):
    info = {}
    with open(bufferlog) as f:
        for line in f:
            if line.startswith("#"): continue
            parts = line.split(' ')
            if len(parts) != 4: continue
            buffer_tag, timestamp, current, buffer_max = parts
            if buffer_tag not in info:
                info[buffer_tag] = []
            info[buffer_tag].append([timestamp, int(current), int(buffer_max)])
    return info

## Ploting functions:

def plot_throughput(client_entries, competing_entries, plot):
    max_y = 0
    for app_entries, label in [(client_entries, "Service A Client"), 
                               (competing_entries, "Competing Flow")]:
        length_by_second = aggregate_by_second(app_entries)
        plot_y = []
        yval = 0; counter = 0
        for sec in range(min_sec, max_sec):
            if counter == SECS_TO_AGGREGATE:
                plot_y.append(yval*8.0/1024)
                counter = 0
                yval = 0
            if sec in length_by_second:
                yval += length_by_second[sec] / SECS_TO_AGGREGATE*1.0
            else:
                yval += 0
            counter += 1
            
        plot.plot([i*SECS_TO_AGGREGATE for i in range(0,len(plot_y))], 
                  plot_y, 
                  lw=2, 
                  label=label)
        max_y = max(plot_y + [max_y])
    return max_y

def plot_bitrate(outgoingFile, plot):
    max_y = 0
    bitratemaps = get_bitratesmap()
    entries = read_outgoing_log(outgoingFile)
    oentries_by_second = {}
    for e in entries:
        oentries_by_second[e.time.s] = e
    plot_y = []
    current_br = 0; counter = 0
    for sec in range(min_sec, max_sec):
        if sec in oentries_by_second:
            current_br = bitratemaps[oentries_by_second[sec].filename] + 64 # audio
        if counter == SECS_TO_AGGREGATE:
            plot_y.append(current_br)
            counter = 0
        counter += 1
    plot.plot([i*SECS_TO_AGGREGATE for i in range(0,len(plot_y))], 
                  plot_y, 
                  lw=2, 
                  label="Requested Bitrate")
    max_y = max(plot_y + [max_y])
    return max_y

def plot_buffer(label, buffer_data, plot):
    occupancy_by_second = {}
    buffer_max = 0
    for timestamp, current, b_max in buffer_data:
        buffer_max = b_max
        sec = int(timestamp.split('.')[0])
        if sec in occupancy_by_second:
            occupancy_by_second[sec] = max(occupancy_by_second[sec], current)
        else:
            occupancy_by_second[sec] = current
    plot_y = []
    current_occupancy = 0
    for sec in range(min_sec, max_sec):
        if sec in occupancy_by_second:
            current_occupancy = occupancy_by_second[sec]
        plot_y.append(current_occupancy)
    plot.plot(range(0,len(plot_y)), plot_y, lw=2, label=label)

def plot_buffers(logsDir):
    bufferlog = os.path.join(logsDir, BUFFER_LOG_FILENAME)
    bufferplotfile = os.path.join(logsDir, BUFFER_PLOT_FILENAME)
    if not os.path.exists(bufferlog):
        return # Not a buffered emulator
    fig = plt.figure()
    plot = fig.add_subplot(1, 1, 1)
    
    buffers_data = get_buffer_info(bufferlog)
    plot_buffer("Video buffer", buffers_data[VIDEO_BUFFER_TAG], plot)
    plot_buffer("Audio buffer", buffers_data[AUDIO_BUFFER_TAG], plot)
    
    plot.set_ybound(-1, buffers_data[VIDEO_BUFFER_TAG][-1][-1] + 5)
    plot.grid(True)
    plot.legend()
    plot.set_xlabel("second")
    plot.set_ylabel("chunks")
    plot.set_title(get_emulator_type(logsDir))
    # Shink current axis's height by 10% on the bottom
    box = plot.get_position()
    plot.set_position([box.x0, box.y0 + box.height * 0.1,
                 box.width, box.height * 0.9])
    # Put a legend below current axis
    plot.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1),
          fancybox=True, shadow=True, ncol=5)
    plt.savefig(bufferplotfile)
    
         
def plot_data(logsDir, remote_ip=None):
    lsofFile = os.path.join(logsDir, LSOF_LOG_FILENAME)
    incomingFile = os.path.join(logsDir, INCOMING_LOG_FILENAME)
    outgoingFile = os.path.join(logsDir, OUTGOING_LOG_FILENAME)
    plotFile = os.path.join(logsDir, PLOT_FILENAME)

    fig = plt.figure()
    plot = fig.add_subplot(1, 1, 1)
    
    if remote_ip == None:
        remote_ip = get_service_ip()
    client_port_list, competing_flow_port_list = grab_ports(lsofFile, remote_ip)
    all_ports = client_port_list + competing_flow_port_list

    entries = read_incoming_log(incomingFile)
    entries_by_port = aggregate_by_ports(entries, remote_ip, all_ports)
    client_entries = []
    competing_entries = []
    for port, p_entries in entries_by_port.items():
        if port in client_port_list: client_entries += p_entries
        elif port in competing_flow_port_list: competing_entries += p_entries
        
    max_y = plot_throughput(client_entries, competing_entries, plot)

    max_b = plot_bitrate(outgoingFile, plot)

    plot.set_ybound(0, math.ceil(max(max_y,max_b)*1.1 / 200)*200)
    plot.grid(True)
    plot.legend()
    plot.set_xlabel("second")
    plot.set_ylabel("kbps")
    plot.set_title(get_emulator_type(logsDir))
    # Shink current axis's height by 10% on the bottom
    box = plot.get_position()
    plot.set_position([box.x0, box.y0 + box.height * 0.1,
                 box.width, box.height * 0.9])
    # Put a legend below current axis
    plot.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1),
          fancybox=True, shadow=True, ncol=5)
    plt.savefig(plotFile)
    
    plot_buffers(logsDir)

if __name__ == "__main__":
    if len(sys.argv) == 3:
        plot_data(sys.argv[1], sys.argv[2])
    elif len(sys.argv) == 2:
        plot_data(sys.argv[1])
    else:
        print "USAGE: python %s LOGS_DIRECTORY [REMOTE_SERVICE_IP_ADDRESS]" % \
              sys.argv[0]
