#!/usr/bin/python

"CS244 Spring 2013 Assignment 3: Netflix : Chunkmap"

import os
import re
import sys

## Regular expressions for log parsing
outgoing_entry_header_re = re.compile(r'^(?P<time_str>\d+.\d{6}) IP (?P<remote_ip>(\d{1,3}\.){4}\d+) > (?P<local_ip>(\d{1,3}\.){4}\d+):.*$')
outgoing_entry_get_re = re.compile(r'^.*GET /(?P<fname>\d+.ismv)/range/(?P<range_start>\d+)-(?P<range_end>\d+)\?.*$')

def log_to_chunkmap(outgoingFile):
    with open(outgoingFile) as f:
        line = "___"
        while line != "":
            line = f.readline()
            header_match = outgoing_entry_header_re.match(line)
            if header_match == None: continue
            http_get_match = None
            while (http_get_match == None and line != ""):
                line = f.readline()
                if line == "\n" or line == "\r\n":
                     line = f.readline()
                     if line == "\n" or line == "\r\n": break
                http_get_match = outgoing_entry_get_re.match(line)
            
            if http_get_match == None: continue
            fname = http_get_match.group("fname")
            start = http_get_match.group("range_start")
            end = http_get_match.group("range_end")
            yield [fname, start, end]
            
def print_chunkmap(outgoingFile):
    for element in log_to_chunkmap(outgoingFile):
        if element[0].endswith('.ismv'):
            print '|'.join(element)

if __name__ == "__main__":
    print_chunkmap(sys.argv[1])
