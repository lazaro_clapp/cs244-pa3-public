#!/bin/sh

capture_time=600
iterations=$(expr $capture_time / 20)

IP=$(python get_my_ip.py)
echo local_ip = $IP

cur_time=$(echo| eval date +%Y%m%d%H%M)
mkdir $cur_time
main_dir=$cur_time

# Function: Fixed Rate Emulator
Fixed_Rate_Emulator() {
    dir=$1
    IP=$2
    iterations=$3

    # Possible bitrates: 235 375 560 750 1050 1750 2350 3000
    for bitrate in 560 1050 2350 3000
    do
        mkdir $dir/$bitrate
        tcpdump -n -tt -A -i en1 src $IP and port 80 and greater 100 > $dir/$bitrate/outgoingLog.txt &
        tcpdump -n -tt -e -i en1 dst $IP and port 80 > $dir/$bitrate/incomingLog.txt &
        sleep 5
     
        # Run Netflix Emulator - fixed rate
        python netflix_emulator.py -d -m constant-bitrate --buffersize 100 --bitrate=$bitrate \
            --write-buffer-usage --buffer-usage-output-file $dir/$bitrate/buffer.log > $dir/$bitrate/output.txt &

        # Check programs using network
        for ((i=0; i <= $iterations; i++))
        do
            lsof -n -i | grep ESTABLISHED >> $dir/$bitrate/lsofLog_tcp.txt &
            sleep 10
        done

        # Start wget download
        python wget_wrapper.py &
        sleep 10

        for ((i=0; i <= $iterations; i++))
        do
            lsof -n -i | grep ESTABLISHED >> $dir/$bitrate/lsofLog_tcp.txt &
            sleep 10
        done
        killall Python
        killall tcpdump
        killall wget
        rm 0*

        python plot_results.py $dir/$bitrate &
    done
}

# Function Old Netflix Emulator
Old_Netflix_Emulator() {
    dir=$1
    IP=$2
    iterations=$3

    tcpdump -n -tt -A -i en1 src $IP and port 80 and greater 100 > $dir/outgoingLog.txt &
    tcpdump -n -tt -e -i en1 dst $IP and port 80 > $dir/incomingLog.txt &
    sleep 5
    
    # Start Netflix Emulator - Old Netflix
    python netflix_emulator.py -d -m old-netflix-step --buffersize 100 \
        --write-buffer-usage --buffer-usage-output-file $dir/buffer.log > $dir/output.txt &
    
    # Check programs using network
    for ((i=0; i <= $iterations; i++))
    do
        lsof -n -i | grep ESTABLISHED >> $dir/lsofLog_tcp.txt &
        sleep 10
    done

    # Start wget download
    python wget_wrapper.py &
    sleep 10
    for ((i=0; i <= $iterations; i++))
    do
        lsof -n -i | grep ESTABLISHED >> $dir/lsofLog_tcp.txt &
        sleep 10
    done
    killall Python
    killall tcpdump
    killall wget
    rm 0*

    python plot_results.py $dir &
}

# Function: Unstable Buffered Emulator
Unstable_Buffered_Emulator() {
    dir=$1
    IP=$2
    iterations=$3

    for buffersize in 20 100
    do
        dir=$1/buffersize_$buffersize
        mkdir $dir

        tcpdump -n -tt -A -i en1 src $IP and port 80 and greater 100 > $dir/outgoingLog.txt &
        tcpdump -n -tt -e -i en1 dst $IP and port 80 > $dir/incomingLog.txt &
        sleep 5
     
        # Start Netflix Emulator - change rate/quality based on buffer occupancy
        python netflix_emulator.py -d -m buffer-sense --buffersize 20 \
            --write-buffer-usage --buffer-usage-output-file $dir/buffer.log > $dir/output.txt &
        for ((i=0; i <= $iterations; i++))
        do
            lsof -n -i | grep ESTABLISHED >> $dir/lsofLog_tcp.txt &
            sleep 10
        done

        # Start wget download
        python wget_wrapper.py &
        sleep 10
        for ((i=0; i <= $iterations; i++))
        do
            lsof -n -i | grep ESTABLISHED >> $dir/lsofLog_tcp.txt &
            sleep 10
        done
        killall Python
        killall tcpdump
        killall wget
        rm 0*

        python plot_results.py $dir &
    done
}

# Stable Buffered Emulator
Stable_Buffered_Emulator() {
    dir=$1
    IP=$2
    iterations=$3

    for buffersize in 20 100
    do
        dir=$1/buffersize_$buffersize
        mkdir $dir

        tcpdump -n -tt -A -i en1 src $IP and port 80 and greater 100 > $dir/outgoingLog.txt &
        tcpdump -n -tt -e -i en1 dst $IP and port 80 > $dir/incomingLog.txt &
        sleep 5
     
        # Start Netflix Emulator - change rate/quality based on the change in buffer occupancy
        python netflix_emulator.py -d -m buffer-sense-smooth --buffersize 100 \
            --write-buffer-usage --buffer-usage-output-file $dir/buffer.log > $dir/output.txt &
        for ((i=0; i <= $iterations; i++))
        do
            lsof -n -i | grep ESTABLISHED >> $dir/lsofLog_tcp.txt &
            sleep 10
        done

        # Start wget download
        python wget_wrapper.py &
        sleep 10
        for ((i=0; i <= $iterations; i++))
        do
            lsof -n -i | grep ESTABLISHED >> $dir/lsofLog_tcp.txt &
            sleep 10
        done
        killall Python
        killall tcpdump
        killall wget
        rm 0*

        python plot_results.py $dir &
    done
}

# Main
for bw in "1500Kbit/s" "5000Kbit/s"
do
    # Set bandwidth pipe with dummynet
    sudo ipfw -q flush
    sudo ipfw add pipe 1 in proto tcp
    sudo ipfw pipe 1 config bw $bw delay 5ms
    sudo ipfw pipe list
    
    cur_directory=$(echo $bw | cut -d '/' -f 1)
    cur_directory=$cur_directory"_s"
    cur_directory=$main_dir/$cur_directory
    mkdir $cur_directory

    dir_fixed=$cur_directory/fixed
    mkdir $dir_fixed
    Fixed_Rate_Emulator $dir_fixed $IP $iterations

    dir_oldN=$cur_directory/old_netflix
    mkdir $dir_oldN
    Old_Netflix_Emulator $dir_oldN $IP $iterations

    dir_buff_unstable=$cur_directory/buffered_unstable
    mkdir $dir_buff_unstable
    Unstable_Buffered_Emulator $dir_buff_unstable $IP $iterations

    dir_buff_stable=$cur_directory/buffered_stable
    mkdir $dir_buff_stable
    Stable_Buffered_Emulator $dir_buff_stable $IP $iterations
    
    echo -en "\007"
    sudo ipfw -f flush
    sudo ipfw -f pipe flush

done
