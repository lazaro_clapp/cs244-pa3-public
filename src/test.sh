#!/bin/sh

capture_time=600
iterations=$(expr $capture_time / 20)

IP=$(python get_my_ip.py)
echo local_ip = $IP

cur_time=$(echo| eval date +%Y%m%d%H%M)
mkdir $cur_time
main_dir=$cur_time

# Function: Fixed Rate Emulator
Fixed_Rate_Emulator() {
    dir=$1
    IP=$2
    iterations=$3

    # Possible bitrates: 235 375 560 750 1050 1750 2350 3000
    for bitrate in 560 2350 3000
    do
        mkdir $dir/$bitrate
        
    done
}

# Function Old Netflix Emulator
Old_Netflix_Emulator() {
    dir=$1
    IP=$2
    iterations=$3

}

# Function: Unstable Buffered Emulator
Unstable_Buffered_Emulator() {
    dir=$1
    IP=$2
    iterations=$3

    for buffersize in 20 100
    do
        dir=$1/buffersize_$buffersize
        mkdir $dir

    done
}

# Stable Buffered Emulator
Stable_Buffered_Emulator() {
    dir=$1
    IP=$2
    iterations=$3

    for buffersize in 20 100
    do
        dir=$1/buffersize_$buffersize
        mkdir $dir

    done
}

# Main
for bw in "1500Kbit/s" "5000Kbit/s"
do
    
    cur_directory=$(echo $bw | cut -d '/' -f 1)
    cur_directory=$cur_directory"_s"
    cur_directory=$main_dir/$cur_directory
    mkdir $cur_directory

    dir_fixed=$cur_directory/fixed
    mkdir $dir_fixed
    Fixed_Rate_Emulator $dir_fixed $IP $iterations

    dir_oldN=$cur_directory/old_netflix
    mkdir $dir_oldN
    Old_Netflix_Emulator $dir_oldN $IP $iterations

    dir_buff_unstable=$cur_directory/buffered_unstable
    mkdir $dir_buff_unstable
    Unstable_Buffered_Emulator $dir_buff_unstable $IP $iterations

    dir_buff_stable=$cur_directory/buffered_stable
    mkdir $dir_buff_stable
    Stable_Buffered_Emulator $dir_buff_stable $IP $iterations
    

done