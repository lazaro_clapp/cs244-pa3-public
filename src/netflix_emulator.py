#!/usr/bin/python

"CS244 Spring 2013 Assignment 3: Netflix"

import httplib
import math
from optparse import OptionParser
import os
import signal
import sys
import time
import numpy
from collections import deque

REQUEST_CONST = "http://198.189.255.2/502473679.ismv/range/0-22715?c=us&n=32&v=3&e=1361613375&t=4inxKIjjKXghJRa8Qa3N44_Fsgs&d=silverlight&p=5.Por-yeMYjYrAGnqgl2QNJNMG3nztnJC6mILlHoAjgec&random=687823873"
AUDIO_BITRATE = "64"
AUDIO_FRAGMENT_SIZE = 16 # In seconds
VIDEO_FRAGMENT_SIZE = 4  # In seconds

## Get bitrates.map file, location relative to this file.
BITRATES_MAP_FILE = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir, "data", "bitrates.map"))
CHUNKS_MAP_FILE = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir, "data", "chunks.map"))
TOKENS_MAP_FILE = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir, "data", "tokens.map"))

def parseCmdLine():
    usage = "usage: %prog [options] netflix_ip"
    parser = OptionParser(usage)
    parser.add_option("-d", "--debug", dest="debugFlag", default=False,
                      action="store_true", help="Enable debugging output.")
    parser.add_option("--debug-output-file", dest="debugFile", default="debug.out",
                      help="Specify a file to which to save the downloaded file. For debugging purposes. Only used if the --debug flag is set.")
    parser.add_option("-m", "--mode", dest="emulatorMode", type="string", 
                      help="Use a specific downloading behavior.", metavar="[full-speed|constant-bitrate|old-netflix|old-netflix-smooth|buffer-sense]")
    parser.add_option("--bitrate", dest="bitRate", default="375", type="string", 
                      help="For emulators using a fixed bitrate.", metavar="[bitrate]")
    parser.add_option("--buffersize", dest="bufferSize", default=20, type="int",
                      help="For emulators using a buffer (this gives the size of the video buffer, the audio buffer is automatically scaled to hold the same amount of media in seconds).", metavar="[size_in_fragments]")
    parser.add_option("--write-buffer-usage", dest="bufferUsageFlag", default=False,
                      action="store_true", help="Enable buffer usage output.")
    parser.add_option("--buffer-usage-output-file", dest="bufferUsageFile", default="buffer.log",
                      help="Specify a file to which to log the usage of the buffer over time. Only used if the --write-buffer-usage flag is set.")
    return parser.parse_args()

class NetflixEmulator:

    def __init__(self, debug=False, debugFile="debug.out"):
        self._bitratesmap = None
        self._chunkmap = None
        self._tokenmap = None
        self._remoteip = None
        self._debug = debug
        if self._debug:
            self._debugOutput = open(debugFile, 'w')
        pass

    def get_bitratesmap(self):
        if self._bitratesmap != None: return self._bitratesmap
        d = {}
        with open(BITRATES_MAP_FILE) as f:
            for line in f:
                line = line.strip()
                if line == "": break
                fname, kbps = line.split('|')
                d[kbps] = fname
        self._bitratesmap = d
        return self._bitratesmap 

    def get_chunk_map(self):
        if self._chunkmap != None: return self._chunkmap
        d = {}
        with open(CHUNKS_MAP_FILE) as f:
            for line in f:
                line = line.strip()
                if line == "": break
                fname, start, end = line.split('|')
                if fname not in d: d[fname] = []
                d[fname].append([start, end])
        self._chunkmap = d
        return self._chunkmap 
        
    def get_token_map(self):
        if self._tokenmap != None: return self._tokenmap
        d = {}
        with open(TOKENS_MAP_FILE) as f:
            for line in f:
                line = line.strip()
                if line == "": break
                fname, token, local_ip, remote_ip = line.split('|')
                self._remoteip = remote_ip
                d[fname] = token
        self._tokenmap = d
        return self._tokenmap

    def get_service_ip(self):
        if self._remoteip != None: return self._remoteip
        self.get_token_map() #get_token_map sets the remote IP as a side effect
        return self._remoteip  

    def start(self, address=None):
        if address == None: address = self.get_service_ip()
        self.conn = httplib.HTTPConnection(address)

    def issue_request(self, filename, start, end):
        token = self.get_token_map()[filename]
        conn = self.conn
        request = "/" + filename + "/range/" + start + "-" + end + "?" + token
        conn.request("GET", request, "", {'Connection': 'Keep-Alive', 
                                          'Accept-Encoding' : 'gzip,deflate,sdch', 
                                          'Accept' : '*/*'})
        response = conn.getresponse()
        if (response.status != httplib.OK) or self._debug:
        	print request
        	print response.status
        r = response.read()
        if self._debug:
            self._debugOutput.write(r)
        for k, v in response.getheaders():
            if k == 'content-length':
                length = v
        if length == None:
            raise Exception("Content-Length header missing from response.")
        return length
    
    def chunk_range_to_bit_range(self, bitrate, chunk_start, chunk_end):
        # return [bit_start, bit_end] where chunk_start.first_bit == bit_start, 
        # chunk_end.last_bit == bit_end
        fname = self.get_bitratesmap()[bitrate]
        chunk_map = self.get_chunk_map()
        start = chunk_map[fname][chunk_start][0]
        end = chunk_map[fname][chunk_end][1]
        return [start, end]
        
    def stop(self):
        self.conn.close()
        if self._debug:
            self._debugOutput.close()

class FragmentBuffer:

    def __init__(self, num_fragments, fragment_duration, logfile=None, tag="buffer"):
        self._size = num_fragments
        self._count = 0
        self._duration = fragment_duration
        self._lastTick = time.time()
        self._lastLogTick = time.time()
        self._logfile = logfile
        self._tag = tag
    
    def update(self):
        newTick = time.time()
        while (self._lastTick + self._duration) < newTick and self._count > 0:
            # Consume a single fragment of duration self._duration from the 
            # buffer
            self._count -= 1
            self._lastTick += self._duration
        
        if self._logfile and (self._lastLogTick < newTick - 1):
            logline = " ".join([self._tag, str(newTick), 
                               str(self._count), str(self._size)])
            self._logfile.write(logline + "\n")
            self._lastLogTick = newTick
    
    def addFragment(self):
        self.update()
        if self._count == 0:
            self._count = 1
            self._lastTick = time.time()
        elif self._count > self._size:
            raise Exception("Buffer Overflow!")
        else:
            self._count += 1
        
    def count(self):
        self.update()
        return self._count
    
    def size(self):
        return self._size
        
    def isFull(self):
        return self.count() == self._size
    
    def isEmpty(self):
        return self.count() == 0


class BufferedNetfilxEmulator(NetflixEmulator):

    def __init__(self, buffersize, buffer_log, debug=False, debugFile="debug.out"):
        NetflixEmulator.__init__(self, debug, debugFile)
        self.buffer_log = buffer_log
        if self.buffer_log:
            self.buffer_log.write("#" + self.getEmulatorDescription() + "\n")
        scale = AUDIO_FRAGMENT_SIZE / VIDEO_FRAGMENT_SIZE
        audio_buffersize = int(math.ceil(1.0*buffersize / scale))
        self.audioBuffer = FragmentBuffer(audio_buffersize, 
                                          AUDIO_FRAGMENT_SIZE, 
                                          logfile=buffer_log, tag="audioBuffer")
        self.videoBuffer = FragmentBuffer(buffersize, 
                                          VIDEO_FRAGMENT_SIZE, 
                                          logfile=buffer_log, tag="videoBuffer")
    
    def getEmulatorDescription(self):
        return "Unknown Emulator"
        
    def get_initial_bitrate(self):
        pass
        
    def get_bitrate(self, chunk_size_bytes, chunk_download_time):
        pass
        
    def run(self):
        bit_rate = self.get_initial_bitrate()
        conn = self.conn
        rates_to_files = self.get_bitratesmap()
        chunk_to_range = self.get_chunk_map()
        total_chunks = len(chunk_to_range[rates_to_files[bit_rate]])
        next_video = 0
        next_audio = 0
        scale = AUDIO_FRAGMENT_SIZE / VIDEO_FRAGMENT_SIZE
        total_audio = math.ceil(1.0*total_chunks/scale)
        while next_video < total_chunks or next_audio < total_audio:
            if not self.audioBuffer.isFull():
                a_start, a_end = self.chunk_range_to_bit_range(AUDIO_BITRATE, next_audio, next_audio)
                self.issue_request(rates_to_files[AUDIO_BITRATE], a_start, a_end)
                self.audioBuffer.addFragment()
                next_audio += 1
            for i in range(scale):
                if not self.videoBuffer.isFull():
                    start_t = time.time()
                    v_start, v_end = self.chunk_range_to_bit_range(bit_rate, next_video, next_video)
                    chunk_size_bytes = self.issue_request(rates_to_files[bit_rate], v_start, v_end)
                    self.videoBuffer.addFragment()
                    next_video += 1
                    end_t = time.time()
                    download_time = end_t - start_t
                    bit_rate = self.get_bitrate(int(chunk_size_bytes), download_time)
                    if next_video == total_chunks:
                        break
            if self.audioBuffer.isFull() and self.videoBuffer.isFull():
                time.sleep(1)
    
    def stop(self):
        NetflixEmulator.stop(self)
        if self.buffer_log:
            self.buffer_log.close()


class NetflixEmulatorFullSpeed(NetflixEmulator):

    def __init__(self, bitrate, debug=False, debugFile="debug.out"):
        NetflixEmulator.__init__(self, debug, debugFile)
        self._bitrate = bitrate
 
    def run(self):
        bit_rate = self._bitrate
        conn = self.conn
        rates_to_files = self.get_bitratesmap()
        chunk_to_range = self.get_chunk_map()
        total_chunks = len(chunk_to_range[rates_to_files[bit_rate]])
        scale = AUDIO_FRAGMENT_SIZE / VIDEO_FRAGMENT_SIZE
        for i in range(total_chunks):
            # Audio
            if i % scale == 0:
                a_start, a_end = self.chunk_range_to_bit_range(AUDIO_BITRATE, i/scale, i/scale)
                self.issue_request(rates_to_files[AUDIO_BITRATE], a_start, a_end)
            # Video
            start, end = self.chunk_range_to_bit_range(bit_rate, i, i)
            self.issue_request(rates_to_files[bit_rate], start, end)


class NetflixEmulatorConstantBitrate(BufferedNetfilxEmulator):

    def __init__(self, bitrate, buffersize, buffer_log, debug=False, debugFile="debug.out"):
        self._bitrate = bitrate
        BufferedNetfilxEmulator.__init__(self, buffersize, buffer_log, debug, debugFile)
    
    def getEmulatorDescription(self):
        return "Constant Bitrate Emulator (Bitrate: %s)" % self._bitrate
 
    def get_initial_bitrate(self):
        return self._bitrate
        
    def get_bitrate(self, chunk_size_bytes, chunk_download_time):
        return self._bitrate

# Observed throughput (kbps) to bitrate (kbps)
# When the 'old netflix' client detects a network throughput of T, it should 
# use the bitrate CBS(a) such that a = max({b < T for b in CBS.keys()})
CONSERVATIVE_BITRATE_SELECTION = {0 : 235, 
                                  1050 : 375, 
                                  1120 : 560, 
                                  1500 : 750, 
                                  1900 : 1050, 
                                  2200 : 1750, 
                                  2500 : 2350, 
                                  3200 : 3000}

class NetflixEmulatorOldNetflix(BufferedNetfilxEmulator):

    def __init__(self, initial_bitrate, buffersize, buffer_log, debug=False, debugFile="debug.out"):
        self._bitrate = initial_bitrate
        BufferedNetfilxEmulator.__init__(self, buffersize, buffer_log, debug, debugFile)
    
    def getEmulatorDescription(self):
        return "Old \"Service A\" Emulator (single throughput measurement)"
 
    def get_initial_bitrate(self):
        return self._bitrate
    
    def get_bitrate(self, chunk_size_bytes, chunk_download_time):
        chunk_size = (chunk_size_bytes * 8.0)/1024
        throughput = chunk_size / chunk_download_time
        now = time.time()
        if self._debug:
            print "Measured throughput (kbps): ", throughput
            self.videoBuffer._logfile.write("%s|Throughput:%s\n" % (now, throughput))
        bitrate = 235
        for t, r in CONSERVATIVE_BITRATE_SELECTION.items():
            if t < throughput:
                bitrate = max(bitrate, r)
        bitrate = str(bitrate)
        if self._debug:
            print "Selected bitrate (kbps): ", bitrate
            self.videoBuffer._logfile.write("%s|Bitrate:%s\n" % (now, bitrate))
        return bitrate

class NetflixEmulatorOldNetflixStep(BufferedNetfilxEmulator):

    def __init__(self, initial_bitrate, buffersize, buffer_log, debug=False, debugFile="debug.out"):
        self._bitrate = initial_bitrate
        self._throughputs = deque(3*[0.0], 3)
        BufferedNetfilxEmulator.__init__(self, buffersize, buffer_log, debug, debugFile)
    
    def getEmulatorDescription(self):
        return "Old \"Service A\" Emulator (multiple throughput measurements)"
 
    def get_initial_bitrate(self):
        return self._bitrate
    
    def get_bitrate(self, chunk_size_bytes, chunk_download_time):
        chunk_size = (chunk_size_bytes * 8.0)/1024
        throughput = chunk_size / chunk_download_time
        self._throughputs.append(throughput)
        conservative_throughput = min(self._throughputs)
        if conservative_throughput == 0: 
            conservative_throughput = throughput
        now = time.time()
        if self._debug:
            print "Measured throughput (kbps): ", throughput
            print "Throughput Queue: " + str(self._throughputs)
            self.videoBuffer._logfile.write("%s|Throughput:%s\n" % (now, throughput))
        bitrate = 235
        for t, r in CONSERVATIVE_BITRATE_SELECTION.items():
            if t < conservative_throughput:
                bitrate = max(bitrate, r)
        bitrate = str(bitrate)
        if self._debug:
            print "Selected bitrate (kbps): ", bitrate
            self.videoBuffer._logfile.write("%s|Bitrate:%s\n" % (now, bitrate))
        return bitrate


class NetflixEmulatorKeepBuffer(BufferedNetfilxEmulator):

    PANIC_MIN = 0.1
    STABLE_MIN = 0.35
    STABLE_MAX = 0.65
    PANIC_MAX = 0.8

    def __init__(self, initial_bitrate, buffersize, buffer_log, debug=False, debugFile="debug.out"):
        self._bitrate = initial_bitrate
        BufferedNetfilxEmulator.__init__(self, buffersize, buffer_log, debug, debugFile)
    
    def getEmulatorDescription(self):
        return "Buffer Sense Emulator (buffer-occupancy directed bitrate)"
 
    def get_initial_bitrate(self):
        return self._bitrate
    
    def get_rate_index(self, bitrate, rates):
        for i in range(0, len(rates)):
            if bitrate == rates[i]: return i
        raise Exception("Unknown bitrate!")
    
    def get_bitrate(self, chunk_size_bytes, chunk_download_time):
        buffer_proportion_full = 1.0*self.videoBuffer.count() / self.videoBuffer.size()
        rates = sorted(
                    [int(k) for k in self.get_bitratesmap().keys() 
                            if k != AUDIO_BITRATE])
        
        rate_index = self.get_rate_index(int(self._bitrate), rates)
        if buffer_proportion_full < NetflixEmulatorKeepBuffer.PANIC_MIN:
            self._bitrate = rates[0]
        elif buffer_proportion_full > NetflixEmulatorKeepBuffer.PANIC_MAX:
            self._bitrate = rates[min(len(rates)-1, rate_index+3)]
        else:
            if buffer_proportion_full < NetflixEmulatorKeepBuffer.STABLE_MIN:
                rate_index = max(rate_index-1, 0)
                self._bitrate = rates[rate_index]
            elif buffer_proportion_full > NetflixEmulatorKeepBuffer.STABLE_MAX:
                rate_index = min(rate_index+1, len(rates)-1)
                self._bitrate = rates[rate_index]
        self._bitrate = str(self._bitrate)
        if self._debug:
            print "Buffer utilization: ", buffer_proportion_full
            print "Selected bitrate (kbps): ", self._bitrate
        return self._bitrate


class NetflixEmulatorKeepBufferSmooth(BufferedNetfilxEmulator):

    ARRAY_SIZE = 3
    THRESHOLD = 2
    PANIC_MIN_PKTS = 2
    PANIC_MAX = 1.0

    def __init__(self, initial_bitrate, buffersize, buffer_log, debug=False, debugFile="debug.out"):
        self._bitrate = initial_bitrate
        self._buffer_proportions = deque(self.ARRAY_SIZE*[0.0], self.ARRAY_SIZE)
        BufferedNetfilxEmulator.__init__(self, buffersize, buffer_log, debug, debugFile)
    
    def getEmulatorDescription(self):
        return "Buffer Sense Emulator (buffer-occupancy-change directed bitrate)"
 
    def get_initial_bitrate(self):
        return self._bitrate
    
    def get_rate_index(self, bitrate, rates):
        for i in range(0, len(rates)):
            if bitrate == rates[i]: return i
        raise Exception("Unknown bitrate!")

    def buffer_delta(self):
         ychange = 0.0
         i = -2
         while ychange == 0 and -i < len(self._buffer_proportions):
             ychange = self._buffer_proportions[i+1] - self._buffer_proportions[i]
             i -= 1
         xchange = -2-i
         return ychange/xchange
    
    def get_bitrate(self, chunk_size_bytes, chunk_download_time):
        buffer_proportion_full = 1.0*self.videoBuffer.count() / self.videoBuffer.size()
        self._buffer_proportions.append(buffer_proportion_full)
        rates = sorted(
                    [int(k) for k in self.get_bitratesmap().keys() 
                            if k != AUDIO_BITRATE])
        
        rate_index = self.get_rate_index(int(self._bitrate), rates)

        num_increasing = 0
        num_decreasing = 0
        for i in range(self.ARRAY_SIZE-1):
            diff = self._buffer_proportions[i+1] - self._buffer_proportions[i]
            if diff < 0:
                num_decreasing+=1
            elif diff > 0:
                num_increasing+=1
        if self.videoBuffer.count() <= self.PANIC_MIN_PKTS:
            self._bitrate = rates[0]
        elif buffer_proportion_full >= self.PANIC_MAX:
            self._bitrate = rates[min(len(rates)-1, rate_index+1)]
        elif num_decreasing - num_increasing >= self.THRESHOLD:
            rate_index = max(rate_index-1, 0)
            self._bitrate = rates[rate_index]
        elif num_increasing - num_decreasing >= self.THRESHOLD:
            rate_index = min(rate_index+1, len(rates)-1)
            self._bitrate = rates[rate_index]
        self._bitrate = str(self._bitrate)
        if self._debug:
            print "Buffer utilization: ", buffer_proportion_full
            print "Delta: ", self.buffer_delta()
            print "Selected bitrate (kbps): ", self._bitrate
        return self._bitrate

def get_buffer_logfile(options):
    if options.bufferUsageFlag:
        return open(options.bufferUsageFile, "w")
    else:
        return None

def start_emulator(options, address=None):
    if options.emulatorMode == 'full-speed':
        emulator = NetflixEmulatorFullSpeed(options.bitRate, 
                                            options.debugFlag, 
                                            options.debugFile) 
    elif options.emulatorMode == 'constant-bitrate':
        buffer_log = get_buffer_logfile(options)
        emulator = NetflixEmulatorConstantBitrate(options.bitRate, 
                                                  options.bufferSize, 
                                                  buffer_log, 
                                                  options.debugFlag, 
                                                  options.debugFile)
    elif options.emulatorMode == 'old-netflix':
        buffer_log = get_buffer_logfile(options)
        emulator = NetflixEmulatorOldNetflix(options.bitRate, 
                                                  options.bufferSize, 
                                                  buffer_log, 
                                                  options.debugFlag, 
                                                  options.debugFile)
    elif options.emulatorMode == 'old-netflix-smooth':
        buffer_log = get_buffer_logfile(options)
        emulator = NetflixEmulatorOldNetflixSmooth(options.bitRate, 
                                                  options.bufferSize, 
                                                  buffer_log, 
                                                  options.debugFlag, 
                                                  options.debugFile)
    elif options.emulatorMode == 'old-netflix-step':
        buffer_log = get_buffer_logfile(options)
        emulator = NetflixEmulatorOldNetflixStep(options.bitRate, 
                                                  options.bufferSize, 
                                                  buffer_log, 
                                                  options.debugFlag, 
                                                  options.debugFile)
    elif options.emulatorMode == 'buffer-sense':
        buffer_log = get_buffer_logfile(options)
        emulator = NetflixEmulatorKeepBuffer(options.bitRate, 
                                                  options.bufferSize, 
                                                  buffer_log, 
                                                  options.debugFlag, 
                                                  options.debugFile)
    elif options.emulatorMode == 'buffer-sense-smooth':
        buffer_log = get_buffer_logfile(options)
        emulator = NetflixEmulatorKeepBufferSmooth(options.bitRate, 
                                                  options.bufferSize, 
                                                  buffer_log, 
                                                  options.debugFlag, 
                                                  options.debugFile)
    else:
        print "Unknown emulator mode."
        exit(1)
    if address:
        emulator.start(address)
    else:
        emulator.start()
    
    def signal_handler(signal, frame):
        print "Termination signal received: clean up."
        emulator.stop()
        sys.exit(0)
    
    signal.signal(signal.SIGINT, signal_handler)  
    signal.signal(signal.SIGTERM, signal_handler)  
    emulator.run()
    emulator.stop()

if __name__ == "__main__":
    (options, args) = parseCmdLine()
    if len(args) > 0:
        start_emulator(options, address=args[0])
    else:
        start_emulator(options)
